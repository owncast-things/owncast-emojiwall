#!/usr/bin/env bash

version_tag="${CI_COMMIT_TAG}"
if [ -z "${version_tag}" ]; then
    version_tag="${CI_COMMIT_SHA}"
fi

echo "exports.version = '${version_tag}';" > ./version.js 

echo "Building Emojiwall Container..."

podman build -t "${container_name}" -t "${container_name}beta" -t "${container_name}nightly" .
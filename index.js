const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);

const {
    Server
} = require("socket.io");
global.io = new Server(server);
const bodyParser = require("body-parser");
const config = require("config");
const emojiWall = require("./src/emoji-wall");
const emojis = require("./src/emojis");
const chatOverlay = require("./src/chat-overlay");
const codeVersion = require("./version.js");
const crypto = require("crypto");
const app_url = getAppUrl()
const sockets_url = `${getAppHttpScheme()}://${config.get('webhooks.host')}:${config.get('webhooks.public_port')}`;
app.set("view engine", "ejs");

app.use(express.static("public"));
app.use(
    bodyParser.urlencoded({
        extended: false,
    })
);
app.use(bodyParser.json());

function getAnalyticsSettings() {
    return {
        'enableAnalytics': config.get('analytics.enable') ?? false,
        'analyticsDomain': config.get('analytics.domain') ?? '',
        'analyticsScriptSource': config.get('analytics.scriptSource') ?? '',
        'analyticsDataEndpoint': config.get('analytics.dataEndpoint') ?? '' 
    }
}

function getAppHttpScheme() {
    // Look for the old 'schema' config parameter, and use that if it's present.
    // Otherwise use the corrected name: scheme.
    let scheme = '';
    if (config.has('webhooks.schema')) {
        console.warn(' Warning: The "schema" config parameter is deprecated. Please use "scheme" instead.');
        scheme = config.get('webhooks.schema');
    } else {
        scheme = config.get('webhooks.scheme');
    }
    return scheme;
}

/**
 * @returns {string} The URL of the app, including the schema, host, port, and prefix.
 * We make the URL as prety as possible, for it is displayed to the user on the homepage.
 **/
function getAppUrl() {
    const scheme = getAppHttpScheme();
    const host = config.get('webhooks.host');
    const port = config.get('webhooks.public_port');
    const prefix = config.get('webhooks.prefix');
    // If host ends with a '/', remove it
    const url_host = host.endsWith('/') ? host.slice(0, -1) : host;
    // If the port is the default for the scheme, don't include it in the URL.
    const url_port = ((port == 80 && scheme == 'http') || (port == 443 && scheme == 'https')) ? '' : `:${port}`;
    let url_prefix = '';
    // If prefix is empty, don't touch it
    if (prefix != '')
        // If prefix doesn't start with '/', add it
        url_prefix = prefix.startsWith('/') ? prefix : `/${prefix}`;

    // Force lowercase on the whole URL just in case
    return `${scheme}://${url_host}${url_port}${url_prefix}`.toLowerCase();
}

/* Routes. */
app.get("/", (req, res) => {
    const token = emojis.generateEmojiToken(10);
    res.set('Cache-Control', 'public, max-age=86400, must-revalidate');
    res.render('homepage', {
        token: token,
        host: config.get('webhooks.host'),
        prefix: config.get('webhooks.prefix'),
        app_url: app_url,
        code_version: codeVersion.version,
        ...getAnalyticsSettings()
    });
});

function buildClientConfig(req) {
    const clientConfig = {
        canvasId: 'particles',
        effectRotation: {}
    }
    const availableParametersMap = {
        'minsize': 'minEmojiSize',
        'maxsize': 'maxEmojiSize',
        'maxcount': 'maxEmojis',
        'mintime': 'minCrossingTime',
        'maxtime': 'maxCrossingTime'
    }
    
    for (const key in availableParametersMap) {
        setClientConfigIntegerParameter(clientConfig, req, key, availableParametersMap[key])
    }
      
    if (getFeatureToggleValue('effect-rotation', req, false)) {
        clientConfig.effectRotation.enabled = true
    }
    return clientConfig;
}

function setClientConfigIntegerParameter(clientConfig, req, queryParameterName, clientConfigParameterName) {
    // We set the client config parameter only if the query parameter is set and is an integer.
    // If not, we completely omit the parameter from the client config, letting the client use its default value.
    let parameterValue;

    if (req.query[queryParameterName] && Number.isInteger(parameterValue = parseInt(req.query[queryParameterName]))) {
        clientConfig[clientConfigParameterName] = parameterValue
    }
}

function getFeatureToggleValue(toggleName, req, defaultValue) {
    const trueValues = ['true', '1'];
    const falseValues = ['false', '0'];
 
    if(trueValues.includes(req.query[toggleName]))
        return true;
    
    if(falseValues.includes(req.query[toggleName]))
        return false;
 
    return defaultValue;
}


// Legacy URL, redirect to feature 'emojiwall'
const user_route = '/:token/:owncast_url_b64';
app.get(user_route, (req, res) => {
    const emojiwall_url = `${config.get('webhooks.prefix')}/${req.params.token}/${req.params.owncast_url_b64}/emojiwall`;
    res.redirect(308, emojiwall_url);
});
// Legacy URL, redirect to feature 'emojiwall'
app.post(user_route, (req, res) => {
    const emojiwall_url = `${config.get('webhooks.prefix')}/${req.params.token}/${req.params.owncast_url_b64}/emojiwall`;
    res.redirect(308, emojiwall_url);
});

// New URL, ending in a feature name: /emojiwall, /chatoverlay
const user_route_with_feature = '/:token/:owncast_url_b64/:feature';
app.get(user_route_with_feature, (req, res) => {

    // Feature - View map
    const feature_map = {
        'emojiwall': 'emoji-wall',
        'chatoverlay': 'chat-overlay'
    }

    // Get view or throw error
    const view = feature_map[req.params.feature];

    const nonce = crypto.randomBytes(16).toString("hex");
    res.setHeader(
        'Content-Security-Policy', `default-src 'self'; img-src *; script-src 'self' 'nonce-${nonce}'; style-src 'self' 'unsafe-inline'; font-src 'self'; frame-src 'self'`
    );
    res.set('Cache-Control', 'public, max-age=86400, must-revalidate');
    res.render(view, {
        images_host: req.params.owncast_url_b64,
        sockets_host: sockets_url,
        sockets_prefix: config.get('webhooks.prefix'),
        app_url: app_url,
        token: req.params.token,
        feature: req.params.feature,
        client_config: buildClientConfig(req),
        nonce: nonce,
        ...getAnalyticsSettings()
    });
});

// New URL, ending in a feature name: /emojiwall, /chatoverlay
app.post(user_route_with_feature, (req, res) => {
    console.log("Received POST request ", req.body);
    /* Possible object types received:
    CHAT	user sends a chat message
    NAME_CHANGED	user changes their username
    USER_JOINED	user joins the chat
    STREAM_STARTED	an incoming RTMP stream is detected
    STREAM_STOPPED	an incoming RTMP stream disconnects (e.g. OBS stops)
    STREAM_TITLE_UPDATED	the title of the stream is updated
    VISIBILITY-UPDATE	a previously sent chat message becomes visible/invisible (set by an Administrator/Moderator)
    */
   switch (req.body.type) {
    case 'CHAT':
        console.log('CHAT');
        handleOwncastChatMessage(req.body, req.params.token, req.params.feature);
        break;
    default:
        console.log('Unknown event type: ', req.body.type);
        break;
    }
    res.json({
        status: 200,
    });
});

app.get('/emoji-font.css', (req, res) => {
    res.type('css');
    res.render('emoji-font_css', {
        app_url: app_url,
    });
});

/**
 * @param {string} message Owncast webhook event as decribed at https://owncast.online/thirdparty/webhooks/#chat
 * @param {string} token
 * @param {string} feature
 */
function handleOwncastChatMessage(message, token, feature) {
    console.log(`Received message for feature ${feature}:`, message, token);
    switch (feature) {
        case 'emojiwall':
            emojiWall.wall(message, token);
            break;
        case 'chatoverlay':
            chatOverlay.overlay(message, token);
            break;
        default:
            console.error(`Unknown feature: ${feature}`);
            break;
    }
}

server.listen(config.get("webhooks.port"), () => {
    console.log("listening on *:" + config.get("webhooks.port"));
    console.log("host: "+config.get("webhooks.host") +", prefix: "+config.get("webhooks.prefix"));
});

function isValidToken(token) {
    return true;
}



io.on("connection", (socket) => {
    console.log(`👋 ${socket.handshake.query.feature} client connected with token ${socket.handshake.query.token}`);
    if (isValidToken(socket.handshake.query.token)) {
    // Join the room named after the token
    socket.join(socket.handshake.query.feature + socket.handshake.query.token);
    }
});

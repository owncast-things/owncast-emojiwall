FROM quay.io/meisam/node:alpine
COPY . .
RUN npm install
ENTRYPOINT ["node", "index.js"]

EXPOSE 5000
LABEL org.opencontainers.image.title="smol.stream"
LABEL org.opencontainers.image.description="Smol stream, but big emoji wall"
LABEL org.opencontainers.image.url="https://smol.stream"
LABEL org.opencontainers.image.authors="Le Fractal"
LABEL org.opencontainers.image.source="https://framagit.org/owncast-things/owncast-emojiwall"
LABEL org.opencontainers.image.documentation="https://framagit.org/owncast-things/owncast-emojiwall"
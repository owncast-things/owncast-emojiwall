# Smol.stream emoji wall for Owncast streamers

The emoji wall (also known as a "on-screen emotes overlay") takes the emojis and custom emotes sent in the chat of [Owncast](https://owncast.online) streamers and displays them bubbling up the screen! 

![smol.stream logo](./public/smol.stream-logo.png)

This is the source code which powers [smol.stream/emojiwall](https://smol.stream/emojiwall), a web site you can use to get your own emoji wall easily (and for free, of course).

But, if you really want to, you can host it yourself using this source code (see ["Self-Hosting the emojiwall"](#self-hosting-the-emojiwall)).

## Release notes

### v1.1.4

#### Fixes

- Homepage: Fix the page for small screens (eg. phones).
- Homepage: Remove trailing whitespace from the "hero token" displayed in the intro paragraph, so it can be selected quickly and accurately.
- Homepage: Link directly to the release in the repo from the footer note.
- Add cache control (1 day) so that clients get the new versions without having to cache-refresh themselves.
- Update some dependencies (config, ejs, jsdom).

### v1.1.3

#### Fixes

- Fixed a bug happening for installs serving at the domain root, which lead to malformed urls and a broken homepage.

### Improvements

- Use the correct SCHEME parameters app-wide instead of SCHEMA. SCHEMA is now deprecated, but still works.
- New smol.stream logo!

### v1.1.2

#### Fixes

 - Correct typos in README.

### v1.1.1

#### Fixes

 - Hotfix: Chrome based browsers now display crisp image emotes too.

### v1.1

#### Fixes

- Image emotes are not smoothed by the browser anymore, making smaller custom emotes look a lot better.
- Emojis now spawn off screen, so that one pixel row is not seen stationary at the bottom of the screen before they start moving.
- All emojis should now only despawn when they're really off-screen.
- Emojis are now prevented to stay partly (or in the worst case: mostly) off screen during the whole transition.
- Added missing regional indicator emojis to the list of supported emojis.
- It's now possible to apply custom CSS to the emoji wall from the properties of the browser source in OBS.

#### Improvements

- Use the new app_url variable app-wide, so that local use is actually possible.

#### New features

- Add analytics support
- User-configurable settings
  - - `minsize`: The minimum size of the emojis in pixels.
    - `maxsize`: The maximum size of the emojis in pixels.
    - `mintime`: The minimum time the emojis take to cross the screen, in seconds.
    - `maxtime`: The maximum time the emojis take to cross the screen, in seconds.
    - `maxcount`: The maximum number of emojis to display at the same time.
    - `effect-rotation`: Set to `1` to make emojis slightly rotate as they travel up the screen.
      Depending on your configuration and the number of emojis, this can be ressource intensive —but it looks great!
      *Example*: `https://smol.stream/emojiwall/🎥🥳💾❤️/aGV5IQ==/?minsize=20&maxsize=40&mintime=5&maxtime=10&effect-rotation=1`

## Self-Hosting the emojiwall

> **The following instructions were heavily based on [Ruffy's excellent blog post](https://blog.rtrace.io/posts/emojiwall-getting-started/#Self-Hosting-the-emojiwall).
Make sure you check it out!**

If you prefer to self-host the **emojiwall**-server, your options are either running it containerized with `podman` (or optionally `docker`) or installing it natively on your server. 

### Containerized emojiwall (podman/docker)

The **emojiwall** is published as container-image to [quay.io](https://quay.io/repository/smolstream/emojiwall). It is built from the [original repository](https://framagit.org/owncast-things/owncast-emojiwall). If you prefer, you can build your own container image from the [Dockerfile](https://framagit.org/owncast-things/owncast-emojiwall/-/blob/main/Dockerfile) in the repository.

```bash
podman container run \
    --name "mycoolemojiwall"
    --detach \
    -p 5000:5000 \
    -e HOSTNAME='smol.stream' \
    -e PREFIX='/emojiwall' \
    -e SCHEME='http' \
    -e PORT='5000' \
    -e PUBLIC_PORT='5000' \
    quay.io/smolstream/emojiwall
```
The container can be configured with environment-variables. The following 3 environment variables are configurable.

- **HOSTNAME** sets a publicly exposed hostname
  - if you want to test it locally, set it to `localhost`
  - if you want to host it accessible from the public, set it to your FQDN (fully qualified domain name)

- **PREFIX** (optional) sets the public path prefix for the URL (e.g., `/myAwesomeEmojiWall`)
  - This is in case you configure your HTTPS reverse proxy to serve your emojiwall not at the root of your domain, but under a specific path. If the emojiwall is available at `https://mydomain.tld/emojiwall`, set `PREFIX` to `"/emojiwall"`. If your emojiwall is served at the root of the domain, leave it empty.

- **SCHEME** whether the emojiwall is shipped via TLS/SSL-secured **HTTPS** or via plain **HTTP**
  - set it to `'https'`, if you expose the **emojiwall** through a reverse-proxy that terminates TLS
  - set it to `'http'`, if you directly access the emojiwall (e.g. from localhost)
  - the **emojiwall** does not support HTTPS directly. This can be done through reverse-proxy

- **PORT** sets the (TCP) port the **emojiwall** is listening on

- **PUBLIC_PORT** sets the (TCP) port the **emojiwall** is served through (e.g. public port of a reverse-proxy)
  - if the **emojiwall** is served through a reverse-proxy the listen is not directly exposed
  - the emojiwall is using this port to rewrite URLs for static content to the public facing port
  - most likely (tcp/80 and/or tcp/443 - however can be a custom port as well)
  - if you're directly accessing the **emojiwall**, just use the same port entered previously at "**PORT**"


### Installation from sources

The **emojiwall** can easily be installed manually from sources, by simply cloning the repository to your machine. The project is written with [node.js](https://nodejs.org/en/) and the awesome [express](https://expressjs.com/) web-framework. So the only prequisite is `npm` and `node` pre-installed. The **emojiwall** does not require a specific `node.js` version - so installing node from your distribution package manager is possible too.

#### Installation

```bash
git clone https://framagit.org/owncast-things/owncast-emojiwall.git
cd ./owncast-emojiwall
npm install
```

#### Configuration

Running the **emojiwall** requires you to have a look into its configuration. The configuration can be found in the file `./config/default.json`. Here's an example configuration.

```json
{
  "webhooks": {
    "host": "emojiwall.rtrace.io",
    "prefix": "",
    "scheme": "http",
    "port": "5000",
    "public_port": "443"
  }
}
```
- **host** sets a publicly exposed hostname
  - if you want to test it locally, set it to `localhost`
  - if you want to host it accessible from the public, set it to your FQDN (fully qualified domain name)

- **prefix** (optional) sets the public path prefix for the URL (e.g., `/myAwesomeEmojiWall`)
  - This is in case you configure your HTTPS reverse proxy to serve your emojiwall not at the root of your domain, but under a specific path. If the emojiwall is available at `https://mydomain.tld/emojiwall`, set `PREFIX` to `"/emojiwall"`. If your emojiwall is served at the root of the domain, leave it empty.

- **scheme** whether the emojiwall is shipped via TLS/SSL-secured **HTTPS** or via plain **HTTP**
  - set it to 'https', if you expose the **emojiwall** through a reverse-proxy that terminates TLS
  - set it to 'http', if you directly access the emojiwall (e.g. from localhost)
  - if you run a public/shared instance please consider utilizing a proper HTTPS setup
  - the **emojiwall** does not support HTTPS directly. This can be done through reverse-proxy
  
- **port** sets the (TCP-) port the **emojiwall** is listening on
  - the only requirement is that this port is accessible
  - be aware that for privileged ports (e.g., tcp/80, tcp/443), the **emojiwall** requires to be run as `root`

- **public_port** sets the (TCP-) port the **emojiwall** is served through (e.g. public port of a reverse-proxy)
  - if the **emojiwall** is served through a reverse-proxy the listen is not directly exposed
  - the emojiwall is using this port to rewrite URLs for static content to the public facing port
  - most likely (tcp/80 and/or tcp/443 - however can be a custom port as well)
  - if you're directly accessing the **emojiwall**, just use the same port entered previously at "**port**"

#### Starting

After all the dependencies of the **emojiwall** were installed, it's time to start our **emojiwall**. Navigate into the root-directory of the repository and invoke `node`.

```bash
node index.js
```

Your **emojiwall** is now successfully started and is listening on the configured TCP-port. When you see log-output that the **emojiwall** is now listening on the configured port, the **emojiwall** is ready to be tested. Fire up your browser of choice, and visit `http://localhost:5000` (assuming you configured the listening port to port 5000 and kept the prefix setting empty).

## Manually sending emojis to your emojiwall

The **emojiwall** is open enough, to not require you to use Owncast at all. You may also manually send emojis to an emojiwall, which makes it perfectly suitable for usage in non-live setups (e.g. OBS recordings). Since the **emojiwall** just listens for incoming webhooks, we can just manually invoke such webhooks ourselves. 

The most lightweight webhook body that is `HTTP POST`'ed from Owncast to the emojiwall looks like this:

```json
{
  "eventData": {
    "body": "❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️"
  }
}
```

This allows you to send emojis to the emojiwall from anywhere.
Here an example with `curl` that allows you to send emojis right from your terminal. 
```bash
curl -X POST --header "Content-Type: application/json" \
     --data '{"eventData":{"body":"😀💙"}}' $EMOJIWALL_URL
```

Alternatively you can also use REST-Clients like [Insomnia REST-Client](https://insomnia.rest/)




[def]: #containerized-emojiwall-podmandocker
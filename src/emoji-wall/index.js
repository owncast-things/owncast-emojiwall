const emojiHelper = require("../emojis");

exports.wall = function(message, token) {
    // Extract emojis.
    const emojiList = emojiHelper.getAllEmojis(message.eventData.body);
    if (emojiList.length > 0) {
        const emojiListString = JSON.stringify(emojiList);
        console.log(`Sending emojis to token ${token}:`, emojiListString.substring(0,25));
        // Send emojis to the clients.
        global.io.to('emojiwall' + token).emit("new_emojis", emojiListString);
    }
}
exports.overlay = function(message, token) {
    console.log(`Sending message to chat overlay with token ${token}`);
    global.io.to('chatoverlay' + token).emit("new_message", message);
}